import Vue from 'vue'
import App from './App'
import Store from './store'

Vue.config.productionTip = false
App.mpType = 'app'

global.Store  = Store
global.State  = Store.state
global.VUEX   = VUEX

const app = new Vue(App)
app.$mount()

// 因为mpvue不支持ref, 所以自行模拟一个_ref临时来用, 仅支持组件, 不支持原生的dom
Vue.mixin({
  props: {
    _ref: {
      type: String,
      default: ''
    }
  },
  methods: {
    _refs(_ref){
      return _.find(this.$children, { _ref })
    }
  }
})
