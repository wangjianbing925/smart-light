function formatNumber (n) {
  const str = n.toString()
  return str[1] ? str : `0${str}`
}

function formatTime (date) {
  const year = date.getFullYear()
  const month = date.getMonth() + 1
  const day = date.getDate()

  const hour = date.getHours()
  const minute = date.getMinutes()
  const second = date.getSeconds()

  const t1 = [year, month, day].map(formatNumber).join('/')
  const t2 = [hour, minute, second].map(formatNumber).join(':')

  return `${t1} ${t2}`
}

function toGuide(){
  const url = /iOS/.test(wx.getSystemInfoSync().system) ? 'https://mp.weixin.qq.com/s/c3s4l94gXSiJrSWIkpflDQ' : 'https://mp.weixin.qq.com/s/XH_nJ4n2CvhPTAvJp2NnDg';
  wx.navigateTo({
    url: '/pages/webview/main?link=' + encodeURIComponent(url)
  })
}


export default  {
  formatTime,
  toGuide,
}
