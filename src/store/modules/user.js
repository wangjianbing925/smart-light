/**
 * 用户模块
 */
import VUEX from '@/store/mutation-types'

export default {
  state: {
    userInfo: null, //  广云平台用户数据
    gizUserInfo: null, // 机智云用户信息
    wxUserInfo: null //微信用户信息
  },
  getters: {},
  mutations: {
    [ VUEX.USER.SET_USER_INFO ](state, payload) {
      state.userInfo = payload;
    },
    [ VUEX.USER.SET_GIZ_USER_INFO ](state, payload) {
      state.gizUserInfo = payload;
    },
    [ VUEX.USER.SET_WX_USER_INFO ](state, payload) {
      state.wxUserInfo = payload;
    }
  },
  actions: {
    getUserData(ctx) {
    }
  }
};
