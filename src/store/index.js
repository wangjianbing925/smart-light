import Vue from "vue";
import Vuex from "vuex";
import login from "./modules/login";
import user from "./modules/user";
import device from "./modules/device";

Vue.use(Vuex);
export default new Vuex.Store({
  state: {},
  mutations: {},
  actions: {},
  modules: {
    login,
    user,
    device
  },
  strict: process.env.NODE_ENV !== "production"
});
