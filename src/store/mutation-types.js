module.exports = {
  LOGIN: {
    SET_TOKEN: "LOGIN.SET_TOKEN"
  },
  USER: {
    SET_GIZ_USER_INFO: "USER.SET_GIZ_USER_INFO",
    SET_USER_INFO: "USER.SET_USER_INFO",
    SET_WX_USER_INFO: "USER.SET_WX_USER_INFO"
  },
  DEVICE: {
    SET_DEVICE_LIST: "DEVICE.SET_DEVICE_LIST",
    SET_CURRENT_DEVICE: "DEVICE.SET_CURRENT_DEVICE",
    SET_CURRENT_ATTRS: 'SET_CURRENT_ATTRS'
  }
};
