const path    = require('path')
const glob    = require('glob')
const _       = require('lodash')

const root    = path.join(__dirname, '../src')
const ext     = '.js'
let pattern = 'pages/**/main.js'

// 遍历pages里面全部的main.js文件，抽出来作为多入口文件，用于小程序
module.exports = function(package = ''){
    if(package){
      pattern = `**/main.js`;
    }
    return _
        .chain(glob.sync(path.join(root, package , pattern)))
        .keyBy()
        .mapKeys(item => item.slice(root.length + 1, item.length - ext.length))
        .mapValues(value => path.resolve(value))
        .value()
}
